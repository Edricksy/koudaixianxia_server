#coding:utf8
'''
Created on 2013-10-12
副本中一波怪物
@author: jt
'''
from app.scense.core.pet.monster import Monster

class MonsterGroup(object):
    ''' 副本中的一组怪物 '''


    def __init__(self,groupCount,data,tag):
        '''初始化 
        @param groupCount: int 第几波怪物
        @param data: []     [同屏数量,出场前停顿秒数,[怪物id,怪物id,怪物id]] 
        @param tag: int 怪物战斗id
        '''
        #[同屏数量,出场前停顿秒数,(怪物id,怪物id,怪物id)]
        self.sscount=data[0]  #同屏数量
        self.stop=data[1]     #停顿秒数
        self.gid=groupCount
        self.mlist={}         #key:怪物战斗id   value:怪物实例
        self.fightNumber=0    #小组战斗力

        for monsterid in data[2]:
            tag+=1
            monster=Monster(monsterid,tag)
            self.mlist[tag]=monster
            self.fightNumber+=monster.fn
        self.tag=tag
    
    def getInfo(self,group):
        '''副本组格式化返回proto'''
        group.sscount=self.sscount
        group.stop=self.stop
        group.gid=self.gid
        for monsterOjb in self.mlist.values():
            monsterinfo=group.mlist.add() #怪物信息
            monsterOjb.getAllPetInfo(monsterinfo)
            
    def getMonster(self,fpetid):
        '''根据战斗id获取怪物实例'''
        return self.mlist.get(fpetid,None)
        