#coding:utf8
'''
Created on 2013-10-8

@author: jt
'''
from firefly.utils.singleton import Singleton
#from twisted.python import log
from app.scense.core.instance.instance import Instance

class InstanceManager(object):
    '''  '''
    __metaclass__ = Singleton


    def __init__(self):
        '''  '''
        self.data={} #key:副本动态id,value:副本实例
        self.tag=1   #副本动态id
        
    def add(self,instanceid,pid=0):
        '''添加一个副本
        @param instanceid: int 副本id
        '''
        instance=Instance(instanceid,self.tag, pid)
        self.data[self.tag]=instance
        self.tag+=1
        return instance
        
    def drop(self,did):
        '''删除副本
        @param did: int 副本动态id
        '''
        if self.data.has_key(did):
            del self.data[did]
            
    def getByid(self,did):
        '''根据副本动态id获取副本信息
        @param did: int 副本动态id
        '''
        instance=self.data.get(did,None)
        return instance
        
    
        
        