#coding:utf-8
'''
Created on 2014-2-20
副本中怪力怪物组 组成的团队
@author: jiangtao
'''

class Team(object):
    
    def __init__(self,owner,info):
        '''
        @param owner: obj 副本类
        @param info: {} 副本信息
        '''
        self.owner=owner;        #副本类
        self.instanceid=owner.id;#副本id
        allgroup=info.get('mlist',None) #副本怪物信息  #[[同屏数量,出场前停顿秒数,[[怪物id,出现方式],[怪物id,出现方式],[怪物id,出现方式]]],[同屏数量,出场前停顿秒数,(怪物id,怪物id,怪物id)]]
        self.group={}      #key:第几波    ，value:组类
        self.groupCount=0  #第几波怪物
        for group in allgroup:
            self.groupCount+=1
            mg=MonsterGroup(self.groupCount,group,self.tag)
            self.group[self.groupCount]=mg
            self.tag=mg.tag
            self.fn+=mg.fightNumber
            self.liveMonsterNumber+=len(mg.mlist) #设置存货怪物的数量
