#coding:utf8
'''
Created on 2013-10-8
副本表
@author: jt
'''
from twisted.python import log
from app.scense.core.pet.monster import Monster
from app.scense.core.pet.pet import Pet
from app.scense.core.user.playermanager import PlayerManager
from app.scense.core.instance.monsterGroup import MonsterGroup
import time
from app.scense.tables import growing_table,modulus_table
from app.scense import tables

class Instance(object):
    '''  '''


    def __init__(self,id,did=0,pid=0):
        '''副本id和角色id'''
        from app.scense.tables import instance_table
        self.id=id   #副本id
        self.did=did #副本动态id
        self.pid=pid #角色id
        self.tag=9999 #怪物战斗id
        self.liveMonsterNumber=0 #活着的怪物数量
        self.liveHeroNumber=0    #活着的英雄(宠物)数量
        self.fn=0    #副本怪物战斗力之和
        self.startTime=int(time.time())
        if pid>0 and did>0:
            player=PlayerManager().getPlayer(pid)
            player.instancedid=did
            self.player=player #副本中的角色实例
            self.liveHeroNumber=len(player.fightPets) #设置存活英雄的数量
        if not instance_table.has_key(self.id):
            log.err("instance_table not find id=%d"%self.id)
            return
        info=instance_table[id] #副本信息
        self.name=info.get("iname",None)  #副本名称
        allgroup=info.get('mlist',None) #副本怪物信息  #[[同屏数量,出场前停顿秒数,[[怪物id,出现方式],[怪物id,出现方式],[怪物id,出现方式]]],[同屏数量,出场前停顿秒数,(怪物id,怪物id,怪物id)]]
        self.group={}      #key:第几波    ，value:组类
        self.groupCount=0  #第几波怪物
        for group in allgroup:
            self.groupCount+=1
            mg=MonsterGroup(self.groupCount,group,self.tag)
            self.group[self.groupCount]=mg
            self.tag=mg.tag
            self.fn+=mg.fightNumber
            self.liveMonsterNumber+=len(mg.mlist) #设置存货怪物的数量
            
        self.silver=info.get('silver',0)  #通关后获得的银币数量
        self.exp = info.get('exp',0)#通关后获得的经验值
        
        
    def attack(self,attid,defid,groupid,killId=0,harm=0):
        '''副本中的攻击
        @param attid: int 进攻者id
        @param defid: int 防守者id
        @param groupid: int 第几波怪物
        @param killId: int 技能id
        @param harm: int 客户端算出的最终伤害值
        '''
        #角色宠物id>=20000,怪物10000<=id<20000
        if attid >= 20000: #宠物
            attObj=self.player.pets.get(attid,None)#宠物实例
#            attObj = Pet(attid) #宠物实例
            defobj = self.group[groupid].mlist[defid] #怪物实例
        else: #怪物
            attObj = self.group[groupid].mlist[defid] #怪物
            defobj=self.player.pets.get(attid,None)#宠物实例
#            defobj = Pet(attid)#宠物实例

        result = self.countMaxHarm(attObj, defobj,killId,harm) #宠物
        return result
        
    def countMaxHarm(self,attObj,defobj,killId,finalHarm):
        '''计算攻击者对防御者的最大伤害
        @param attObj: instance 攻击者实例
        @param defobj: instance 防御者实例
        @param killId: int 攻击者使用的技能id
        @param finalHarm: int 客户端计算出来的最终伤害值
        '''
        if not attObj or not defobj:
            print "core.instance.countMaxHarm(param is None) attObj:"+str(attObj)+"  defobj:"+str(defobj)
        modulus=tables.modulus_table #系数、标准
#        #闪避率
#        sbl=1-1/(1+(defobj.agl-attObj.dex)*modulus['agl']/modulus['hp'])
#        #格挡率
#        gdl=defobj.gdl
#        #暴击率
#        bjl=2*(attObj.cri-defobj.tou)*modulus['cri']/modulus['att']
        #暴击威力
        bjwl=1.5+2*(attObj.crp-defobj.ske)*modulus['crp']/modulus['att']
        #伤害率
        defobj.harml
        #伤害值
        attObj.harm
        harm=attObj.harm*defobj.harml*bjwl
        skillInfo=tables.skill_table.get(killId) #技能信息
        if skillInfo:
            exec(skillInfo['script'])
        if finalHarm<=harm:
            if defobj.lowerHp(finalHarm)<=0:
                if defobj.ispet==True: #是否是英雄
                    self.liveHeroNumber-=1
                else:
                    self.liveMonsterNumber-=1
        return harm

    
    def countHarm(self,attObj,defobj):
        '''计算攻击者对防御者的正常伤害
        @param attObj: instance 攻击者实例
        @param defobj: instance 防御者实例
        '''
        modulus=tables.modulus_table #系数、标准
        
        #闪避率
        sbl=1-1/(1+(defobj.agl-attObj.dex)*modulus['agl']/modulus['hp'])
        
        #格挡率
        gdl=2*defobj.par*modulus['par']/modulus['hp']
        
        #暴击率
        bjl=2*(attObj.cri-defobj.tou)*modulus['cri']/modulus['att']
        
        #暴击威力
        bjwl=1.5+2*(attObj.crp-defobj.ske)*modulus['crp']/modulus['att']
        
        #伤害率
        defobj.harml
        
        #伤害值
        attObj.harm
        
        
        harm=attObj.harm*defobj.harml
        return harm
    

    def getinfo(self,rsp):
        '''获取副本信息
        @param rsp: obj  proto类实例
        '''
        rsp.result=True
        info=rsp.info        #副本信息
        info.id=self.id      #副本id
        info.name=self.name  #副本名称
        info.silver=self.silver     #通关后获得的银币数量
        for monsterGroup in self.group.values():
            group=info.grouplist.add()  #副本中怪物组
            monsterGroup.getInfo(group)