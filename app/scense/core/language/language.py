#coding:utf8
'''
Created on 2012-2-21

@author: jt
'''
from app.scense.core.singleton import Singleton
from twisted.python import log



class Lg:
    '''语言类'''
    __metaclass__ = Singleton

    def __init__(self):
        '''初始化'''
        from app.scense.tables import language_table
        #self.info={}#key:表id ， value:翻译之后的内容
        self.info=language_table
        
    #def update(self):
    #    '''更新数据信息'''
    #    self.info=dbLanguage.getAll()
        
    def g(self,id):
        '''根据id获取翻译信息'''
        try:
            info= self.info.get(id)
            if not info:
                log.err(u"%s,Lg not find id : "%id)
                return str(id)
            return info
        except:
            log.err(u"%s,Lg not find id : "%id)
            return str(id)