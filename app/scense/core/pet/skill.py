#coding:utf8
'''
Created on 2013-9-23

@author: jt
'''
from app.scense.tables import skill_table 

class Skill(object):
    ''' 宠物技能 '''


    def __init__(self,id):
        ''' 初始化宠物技能 
        @param id: int skill表主键id
        '''
        self.id=id    #技能表主键id
        info=skill_table[id]
        self.name=info['kname']    #技能名称
        self.typeid=info['typeid']  #技能类型 1射击 2弹弹球 3圆形范围 4切水果
        self.script=info['script']  #伤害计算公式
        self.counts=info['counts']  #每次允许发射次数 x表示触发x次技能之后技能冷却
        self.ranges=info['ranges']  #每一次触发技能后 碰触墙壁次数、半径、伤害次数
        self.times=info['times']    #技能冷却时间 秒数
        self.buffid=info['buffid']  #触发的buff表的id 0为没有buff
        self.cri=info['cri']        #暴击概率 70表示暴击概率为70%
        self.crp=info['crp']        #暴击威力
        self.nextid=info['nextid']  #本技能升级后的技能id
        self.gold=info['gold']      #技能升级所需要的金币
        self.attackeffect=info['attackeffect']    #技能释放特效
        self.moveeffect=info['moveeffect']        #技能移动特效
        self.passiveeffect=info['passiveeffect']  #受击特效：怪物收到此技能攻击的时候的特效
        
    def getSkillInfo(self,skl):
        '''获取技能信息
        @param skl: obj proto
        '''
        skl.id=self.id
        skl.name=self.name
        skl.typeid=self.typeid
        skl.script=self.script
        
        skl.counts=int(self.counts)
        skl.ranges=int(self.ranges)
        skl.times=int(self.times)
        skl.buffid=int(self.buffid)
        skl.cri=float(self.cri)
        skl.crp=float(self.crp)
        
        skl.nextid=self.nextid
        skl.gold=self.gold
        skl.attackeffect=self.attackeffect
        skl.moveeffect=self.moveeffect
        skl.passiveeffect=self.passiveeffect
        