#coding:utf8
'''
Created on 2013-9-26

@author: jt
'''
from app.scense.core.user.playermanager import PlayerManager
from app.mem.mPlayer import playerPet_m
from app.scense.core.language.language import Lg

def getAllPetInfo(pid,rsp):
    '''获取角色所有宠物信息
    @param pid: int 角色id
    @param rsp: obj proto
    '''
    player=PlayerManager().getPlayer(pid)
    rsp.result=True
    rsp.psinfo=[]
    if not player:
        rsp.result=False
        rsp.message=u'没有找到这个角色'
        return
    player.getAllPetInfo(rsp)
    
def addPet(pid,petid,rsp):
    '''添加一个宠物'''
    player=PlayerManager().getPlayer(pid)
    rsp.result=True
    if not player:
        rsp.result=False
        rsp.message=u'没有找到这个角色'
        return 
    flg=player.addPet(petid)
    rsp.result=flg
    

def getPlayerPets(pid,rsp):
    '''获取角色出战列表详细信息
    @param pid: int 角色id
    @param rsp: obj proto
    '''
    player=PlayerManager().getPlayer(pid)
    rsp.result=True
    rsp. pfinfo=[]
    if not player:
        rsp.result=False
        rsp.message=u'没有找到这个角色'
        return
    player.getFightPetsInfo(rsp)
    
    
    
    
def movePet(pid,moveid,targetid,rsp):
    '''将宠物移入备战区
    @param pid: int 角色id
    @param moveid: int 列表中的角色宠物id
    @param targetid: int 备战区位置 1-4
    '''
    player=PlayerManager().getPlayer(pid)
    rsp.result=True
    if not player:
        rsp.result=False
        rsp.message=u'没有找到这个角色'
    player.addFightPet(moveid,targetid,rsp)
    
def moveHalt(pid,moveid,targetid,rsp):
    '''宠物从备战区移动到休息区
    @param pid: int 角色id
    @param moveid: int 列表中的角色宠物id
    @param targetid: int 备战区位置 1-4
    '''
    player=PlayerManager().getPlayer(pid)
    rsp.result=True
    if not player:
        rsp.result=False
        rsp.message=u'没有找到这个角色'
    player.addFightPet(moveid,targetid,rsp)
    
def unlockPet(pid,ppid,rsp):
    '''花钱解锁宠物
    @param pid: int 角色id
    @param ppid: int 角色宠物id player_pet表主键id
    '''
    player=PlayerManager().getPlayer(pid)
    if not player:
        rsp.result=False
        rsp.msg=Lg().g(1014)
    ppObj=playerPet_m.getObj(ppid)
    data=ppObj.get("data")
    if data['unlocks']==1:
        rsp.result=False
        rsp.msg=Lg().g(1013)
        return
    silver=data['silver']#花费的银币
    flag=player.delSilver(silver)
    if not flag:
        rsp.result=False
        rsp.msg=Lg().g(1004)
        return
    ppObj.update('unlocks', 1)
    ppObj.syncDB()
    rsp.result=True