#coding:utf8
'''
Created on 2013-9-22
玩家登陆   gate服务传递过来的参数
@author: jt
'''
from firefly.server.globalobject import remoteserviceHandle
from app.scense.core.user.playermanager import PlayerManager
from app.mem.mPlayer import playerPet_m, playerFight_m, playerWelfare_m
from app.scense.tables import welfareOrder
from app.db.player_start_pets import getStartPets

print "load func"

@remoteserviceHandle('gate')
def land_1000(did,uname):
    '''玩家登陆
    @param did: int 登陆动态id
    @param uname: str 角色账号
    '''
    PlayerManager().addPlayerByUname(uname)
    
@remoteserviceHandle('gate')
def landUPdate_1001(did,pid):
    '''更改玩家登陆动态id'''
    flg=PlayerManager().updatePlayerDid(did, pid)
    return flg

@remoteserviceHandle('gate')
def landUPdate_1002(did,uname,pid):
    '''玩家注册时数据库添加的数据'''
    #firstPetId=3 #玩家拥有的第一个宠物的id
    petlist=getStartPets()
    nppidlist=[]
    for firstPetId in petlist:
        ppm=playerPet_m.new({'pid':pid,'petid':firstPetId,'level':1,'exp':1,'unlocks':1,'silver':0,'newgid':0,'qhhplv':0,'qhattlv':0})
        ppm.syncDB()
        nppid=ppm.data['id']
        nppidlist.append(nppid)
    data="{"
    i=0;
    for item in nppidlist:
        i+=1
        data+="%d:%d,"%(i,item)
    for t in range(4-i):
        i+=1
        data+="%d:0,"%i
    data=data[0:len(data)-1]
    data+="}"
    pf=playerFight_m.new({'pid':pid,'fightpet':data})
    pf.syncDB()
    
    for k in welfareOrder.keys():
        datat={'pid':pid,'typeid':k,'progress':0,'counts':0,'state':0}
        pw=playerWelfare_m.new(datat)
        pw.syncDB()
    PlayerManager().addPlayerByUname(uname)

@remoteserviceHandle('gate')
def land_1004(did,pid):
    '''玩家退出登陆
    @param did: int 登陆动态id
    @param pid: int 玩家id
    '''
    PlayerManager().dropBypid(pid)
    


def dl(uname):
    PlayerManager().addPlayerByUname(uname)