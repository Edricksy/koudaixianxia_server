#coding:utf8
'''
Created on 2013-11-7

@author: ywq
'''

from firefly.server.globalobject import remoteserviceHandle
#from app.scense.protofile.intensify import GetIntensifyInfo1401_pb2
from app.scense.applyInterface.welfare_app import getAllWelfareInfo1501,getReward1502
from app import js


@remoteserviceHandle('gate')
def getIntensifyInfo_1501(did,data):
    '''获取福利界面信息
    '''
    info = js.load(data)
    pid = info.get("pid")
    response = js.Obj()
    getAllWelfareInfo1501(pid,response)
    tt=js.objstr(response)
    return js.objstr(response)

@remoteserviceHandle('gate')
def getReward_1520(did,data):
    '''领取福利
    '''
    info = js.load(data)
    pid = info.get("pid")
    typeid = info.get("typeid")
    response = js.Obj()
    data = getReward1502(pid,typeid)
    response.result = data
    tt=js.objstr(response)
    return js.objstr(response)