#coding:utf8
'''
Created on 2013-9-17
角色相关
@author: jt
'''

from firefly.dbentrust.mmode import MAdmin
import time


#角色表
player_m = MAdmin('player','id',timeout=600)
player_m.insert()

#角色拥有的宠物表
playerPet_m = MAdmin('player_pet','id',incrkey='id',timeout=600)
playerPet_m.insert()


#宠物出战列表
playerFight_m = MAdmin('player_fight','pid',timeout=600)
playerFight_m.insert()


#角色通关记录
playerInstance_m=MAdmin('player_instance','pid',timeout=600)
playerInstance_m.insert()


#角色福利任务记录表
playerWelfare_m = MAdmin('player_welfare','id',timeout=600)
playerWelfare_m.insert()
